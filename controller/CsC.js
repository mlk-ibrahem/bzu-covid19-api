const {CountryModel} =require('../models/country'); 
const logger = require('../services/logger');
const cache =require('../services/cache'); 
const redisCache = require('../services/rides');




module.exports.CountriesCotrol =  async (req , res)=>{
    
    //var cached_data = cache.get("countries_status");
    var cached_data = await redisCache.get("countries_status");
    if (cached_data != undefined){
        logger.info("/api/countries/status  this api is cached ")
        res.send({ data : cached_data , message : " match found"});
        return;
    }
    CountryModel.find((err, countries)=>{
        if (err) {
            res.statusCode(404).json({msg : " the status is error " }) 
        }
                if(countries.length == 0 ){
                    res.send({data : [] , message : "no mathc file "})
                    return; 
                }

                //cache data 
                cache.set("countries_status" , countries , 60);
                redisCache.set("countries_status" , countries );
                logger.info("/api/countries/status  data stored is cached   ")
                res.send({ data : countries , message : " match found"})
            })
        } 

module.exports.CacheRemove = async (req , res)=>{
    await redisCache.del("countries_status");
    res.send("Cacheing remove ")
}; 

