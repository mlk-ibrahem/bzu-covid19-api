const {CoordinateModel} = require('../models/coor'); 
const logger = require('../services/logger');
const cache =require('../services/cache'); 
const redisCache = require('../services/rides');



module.exports.codeeController = (req , res)=>{
    var cached_data_codes = cache.get("codes");
    if (cached_data_codes != undefined){
        logger.info("/api/countries/codes   this api is cached ")
        res.send({ data : cached_data_codes , message : " match found"});
        return;
    }   

    var _results = []; 
    CoordinateModel.find(function(err, coordinates){
        if (err) {
            res.statusCode(404).json({msg : " no country found" }) 
        }
         //cache data 
         cache.set("codes" , coordinates , 60);
         redisCache.set("codes" , coordinates );
         logger.info("/api/countries/codes  data stored is cached   ")
         _results= coordinates;
         res.send({ data : coordinates , message : " match found"})
      
    })

}