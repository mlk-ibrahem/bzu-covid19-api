
const {CountryModel} =require('../models/country'); 
const logger = require('../services/logger');
const cache =require('../services/cache'); 
const redisCache = require('../services/rides');


module.exports.Iso2_cont = (req,res)=>{
    
    var cached_data_iso2 = cache.get("iso2_code");
    if (cached_data_iso2 != undefined){
        logger.info("/api/countries/status/:iso2_code  this api is cached  ")
        res.send({ data : cached_data_iso2 , message : " match found"});
        return;
    }
    const code = req.params.iso2_code.toUpperCase();
    CountryModel.find({iso2_code : code},(err, countries)=>{
        if(err){
            res.send({error:1 , message: "application error"});
            return;
        }
        if(countries.length == 0 ){
            res.send({data : [] })
            return; 
        }
        //cache data 
        cache.set("countries_status" , countries , 60);
        redisCache.set("countries_status" , countries );
        logger.info("/api/countries/status/:iso2_code  data stored is cached   ")
        res.send({data : countries })
            });
        }