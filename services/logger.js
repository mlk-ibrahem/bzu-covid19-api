const winston = require('winston');
require('winston-mongodb'); 
const expressWinston = require('express-winston');


// this winston to have a type of error and print the erorr at the file error.log 
const logger = winston.createLogger({

    level:'info', 
    format: winston.format.json(), 
    transports: [
        new winston.transports.Console(
            {
            format: winston.format.combine(
                winston.format.colorize(),
                winston.format.json()
              )
            }),
        new winston.transports.File(
            { filename: 'error.log', 
            level: 'info',//the type will be error to sho the error have at the application 
            format: winston.format.combine(winston.format.timestamp(),winston.format.json(),winston.format.colorize())
       
        }),
        
        
        
        //if we need to print the error or the 'level' at the db we ned to conect the db 
        //we use the packige winston-mongodb i install to conect and print the error at the db 
        ///////////////////////////////////////////// 
        new winston.transports.MongoDB(
            {  
            level: 'erorr',//this livel will be errorOr any livel to sho the error have at the application 
            options: { useNewUrlParser: true,
                      useUnifiedTopology: true},
            db: 'mongodb://heroku_zff15q0n:hoc7ol0hnkb7b2dvgm8l7gkvre@ds011462.mlab.com:11462/heroku_zff15q0n',
            }),         
                ]
        }); 



 module.exports = logger ; 