const mongoose= require('mongoose'); 

const coorSchema= new mongoose.Schema({
    region_code : {type:Number},
    region_name : {type:String},
    sub_region_code : {type:Number},
    sub_region_name  : {type:String},
    intermediate_region_code : {type:Number}, 
    intermediate_region_name : {type:String},
    google_map_name : {type:String},
    un_name : {type:String},
    m49_code : {type:Number}, 
    iso2_code : {type:String},
    iso3_code : {type:String},
    latitude: {type:Number}, 
    longitude: {type:Number},
    schengen_country: {type:Number},
    europe_union : {type:Number}
},{collection:'coordinates'});



const CoordinateModel= mongoose.model('Coordinate', coorSchema);

module.exports.CoordinateModel = CoordinateModel;
module.exports.CoordinationSchema = coorSchema; 