const mongoose= require('mongoose'); 


const countrySchema = new mongoose.Schema({
    travel_level : {type:Array},
    country: {type:String},
    region : {type:String},
    latitude : {type:Number},
    longitude: {type:Number},
    iso2_code: {type:String},
    iso3_code: {type:String},
    closed_border: {type:String},
    covid_score: {type:Object},
    update_date: {type:String},
    countries : {type:Object}
},{collection:'countries'});



const CountryModel= mongoose.model('Country', countrySchema);
module.exports.CountryModel = CountryModel;
module.exports.countrySchema = countrySchema; 