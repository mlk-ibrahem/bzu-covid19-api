const express=require('express');
const expressWinston = require('express-winston');
const morgan= require('morgan'); 
const bodyParser=require('body-parser');
const compression =require('compression'); 
const app =express(); 
const port =3000 

app.use(compression()); 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(morgan('combined'));  
app.use(morgan('dev'));


const db = require('./services/db');
const router = require('./services/Router'); 
app.use(router);

app.listen(port , ()=>{console.info('the application it work at port 3000 ', 'http;//localhost:${port}');}); 
